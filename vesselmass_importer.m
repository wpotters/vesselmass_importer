classdef vesselmass_importer
    % obj = vesselmass_importer(filename)
    %
    % to get coordinates: 
    % (1) obj = vesselmass_importer(filename); % load file
    % (2) coordinates2D = obj.getContourCoordinates();   % 2d x,y
    % (3) coordinates3D = obj.getContourCoordinates3d(); % 3d with slice#

    % --- Author information
    % Wouter Potters
    % Academic Medical Center, Amsterdam, The Netherlands
    % w.v.potters@amc.nl
    % Date: 25-January-2015
    
    properties
        filename; % con file that was used
        fileversion; 
        
        invert_slice_order = false; % bool to invert the slice order
                                    % in case the manufacturer provided
                                    % wrong slice ordering
        
        % data from file
        settings = struct('main_properties',[],...
                          'study_information',[],...
                          'mass_mode',[],...
                          'quantification_mode',[],...
                          'contour_detection',[],...
                          'display_local_info',[],...
                          'display_global_info',[],...
                          'stop',[])
        roi_info = struct('num_rois',[]);
        contours = [];
    end
    
    properties (Access = private)
        % used while reading the file
        fid = -1;
        current_section = '';
        current_property_tree = 'obj';
        current_contour_id = 0;
        encoding_matrix_coordinates = [5 4 3; 6 NaN 2; 7 0 1]; % translation table for coordinates 5 = x:-1;y-1 etc.
    end
    
    methods
        % constructor - checks input file name and imports the data
        function obj = vesselmass_importer(varargin)
            if nargin == 1 % filename was provided
                obj.filename = varargin{1};
            end
            obj = obj.importfile();
        end
        
        % check if input filename is correct.
        function obj = set.filename(obj,filename)
            if exist(filename,'file')~=2
                error('filename is invalid')
            end
            [~,~,ext] = fileparts(filename);
            if ~strcmp(ext,'.con')
                error('filename is not a *.con file.')
            end
            obj.filename = filename;
        end
        
        % reimport file on change of this setting.
        function obj = set.invert_slice_order(obj,value)
            old = obj.invert_slice_order;
            new = (value == true);
            if old ~= new
                obj.invert_slice_order = new;
                obj.current_contour_id = 0;
                obj = obj.importfile();
            end
        end
        
        % outputs the coordinates of all available roi's in 2D
        function contour_coordinates = getContourCoordinates(obj,varargin)
            % contour_coordinates = getContourCoordinates(obj)
            % gets coordinates in 2d
            if nargin == 1
                addSliceNr = false;
            elseif nargin == 2
                addSliceNr = varargin{1} == true;
            end
            all_rois = cellfun(@(x) str2double(x),{obj.roi_info.roi.roi_id});
            available_rois = find(ismember(all_rois,unique([obj.contours.contour_type_id])));
            available_roi_names = {obj.roi_info.roi(available_rois).roi_label};
            contour_coordinates = struct();
            for iroi = available_rois
                contour_index = 0;
                for icont = 1:length(obj.contours)
                    if (obj.contours(icont).contour_type_id == all_rois(available_rois(iroi)))
                        contour_index = contour_index+1;
                        if addSliceNr
                            contour_to_add = [ obj.contours(icont).coordinates;...
                                               repmat(obj.contours(icont).slice_nr,1,obj.contours(icont).nr_coords)];
                        else
                            contour_to_add = [obj.contours(icont).coordinates];
                        end
                        contour_coordinates.(available_roi_names{iroi}){contour_index} = contour_to_add;
                    end
                end
            end
        end
        
        % outputs the coordinates of all available roi's in 3D
        function contour_coordinates = getContourCoordinates3d(obj)
            % contour_coordinates = getContourCoordinates(obj)
            % gets coordinates in 3d (3rd dim is slice#)
            contour_coordinates = getContourCoordinates(obj,true);
        end
        
    end
    
    methods (Access = private)
        % loop over entire file
        function obj = importfile(obj)
            % obj = importfile(obj)
            % loops over the entire *.con file to import it
            try 
                obj.fid = fopen(obj.filename);
                
                obj = obj.setVersion(fgetl(obj.fid)); % first line is always the fileversion
                
                while ~feof(obj.fid) % read until end of file

                    line = fgetl(obj.fid);     % get current line
                    
                    if strcmp(line(1),'[') % new section detected; process it
                        obj = obj.process_new_section(line);
                    else
                        obj = obj.process_current_section_contents(line);
                    end
                    
                end
                
                fclose(obj.fid); 
            catch errmsg
                try fclose(obj.fid); catch err, warning(['File could not be closed: ' err.message]); end % always close the file
                error(['Importing .con file failed:' errmsg.message]);
            end
        end
        
        % define new section base on text between brackets
        function obj = process_new_section(obj,line)
            % obj = process_new_section(obj,line)
            switch line
                case '[MAIN]'
                    obj.current_section = 'main';
                    obj.current_property_tree = 'obj.settings.main_properties';
                case '[STUDY_INFORMATION]'
                    obj.current_section = 'study_information';
                    obj.current_property_tree = 'obj.settings.study_information';
                case '[MASS_MODE]'
                    obj.current_section = 'mass_mode';
                    obj.current_property_tree = 'obj.settings.mass_mode';
                case '[QUANTIFICATION]'
                    obj.current_section = 'quantification';
                    obj.current_property_tree = 'obj.settings.quantification';
                case '[CONTOURDETECTION]'
                    obj.current_section = 'contour_detection';
                    obj.current_property_tree = 'obj.settings.contour_detection';
                case '[DISPLAY GLOBAL INFO]'
                    obj.current_section = 'disp_global_info';
                    obj.current_property_tree = 'obj.settings.display_global_info';
                case '[DISPLAY LOCAL INFO]'
                    obj.current_section = 'disp_local_info';
                    obj.current_property_tree = 'obj.settings.display_local_info';
                case '[ROI_INFO]'
                    obj.current_section = 'roi_info';
                    obj.current_property_tree = 'obj.roi_info';
                case '[FCONTOUR]'
                    obj.current_section = 'contours';
                    obj.current_property_tree = 'obj.contours';
%                 case '[END ROI_INFO]'     % skip this one - empty
%                 case '[STOP]'             % skip this one - empty
                otherwise 
                    if ~any(strcmpi({'[END ROI_INFO]','[STOP]'},line))
                        warning(['section ''' line ''' was skipped']);
                        % no warnings for END and stop stuff
                    end
                    obj.current_section = 'skip_this_section';
                    obj.current_property_tree = 'obj';
            end
        end
        
        % fill contents of current section until new section is found
        function obj = process_current_section_contents(obj,line)
            % obj = process_current_section_contents(obj,line)
            switch obj.current_section
                case 'skip_this_section'
                    % do nothing; session is skipped
                case 'roi_info'
                    [property_name,property_value] = splitPropertyValue(line);
                    if strcmp(property_name,'num_rois')
                        obj.roi_info.num_rois = str2double(property_value);
                        obj.roi_info.roi = repmat(struct(),1,obj.roi_info.num_rois);
                    else
                        loc_undsc = strfind(property_name,'_');
                        loc_dot   = strfind(property_name,'.');
                        current_roi_id = str2double(property_name(loc_undsc+1:loc_dot-1))+1; % avoid 0; hence +1
                        
                        if isempty(fieldnames(obj.roi_info.roi(current_roi_id))) || isempty(obj.roi_info.roi(current_roi_id).roi_id)
                            obj.roi_info.roi(current_roi_id).roi_id = property_name(loc_undsc+1:loc_dot-1);
                        end
                        obj.roi_info.roi(current_roi_id).(property_name(loc_dot+1:end)) = property_value;
                    end
                case 'contours'
                    % automatically read in the next 5 lines as well.
                    obj.current_contour_id = obj.current_contour_id+1;
                    lines = {line};
                    for iline = 1:5
                        if feof(obj.fid), error('Error. Con file seems incomplete. Reached end of during contour reading.'); end
                        lines{iline+1} = fgetl(obj.fid);
                    end
                    line1_splitted = str2double(strsplit(lines{1},' '));
                    % line 1
                    if obj.invert_slice_order
                        nrofslices = sum(cellfun(@(x) ~isempty(x) && x == 1,regexp(fieldnames(obj.settings.contour_detection.selection),'slice_.*')));
                        obj.contours(obj.current_contour_id).slice_nr        = nrofslices+1 - line1_splitted(1); % somehow the slice_nrs / order are inverted in the DICOM format.
                    else
                        obj.contours(obj.current_contour_id).slice_nr        = line1_splitted(1); % somehow the slice_nrs / order are inverted in the DICOM format.
                    end
                    obj.contours(obj.current_contour_id).unknown_param1  = line1_splitted(2);
                    obj.contours(obj.current_contour_id).contour_type_id = line1_splitted(3);
                    obj.contours(obj.current_contour_id).scaling         = line1_splitted(4);
                    % line 2
                    obj.contours(obj.current_contour_id).nr_coords       = str2double(lines{2});
                    % line 3
                    obj.contours(obj.current_contour_id).coord_start     = str2double(strsplit(lines{3},' '));
                    % line 4
                    obj.contours(obj.current_contour_id).coord_offsets   = lines{4};
                    obj = obj.getCoords();
                    % line 5
                    obj.contours(obj.current_contour_id).unknown_param5  = lines{5};
                    % line 6
                    obj.contours(obj.current_contour_id).unknown_param6  = lines{6};
                    % other stuff
                    roi_names = {obj.roi_info.roi.roi_label};
                    obj.contours(obj.current_contour_id).contour_type    = roi_names{cellfun(@(x) str2double(x), {obj.roi_info.roi.roi_id}) == obj.contours(obj.current_contour_id).contour_type_id};                    
                case {'main','quantification','mass_mode','study_information','contour_detection','disp_global_info','disp_local_info'}
                    append_this_1 = strrep((regexprep(line,'(.*)=(.*)','$1')),' ','_');
                    append_this_2 = (regexprep(line,'(.*)=(.*)','$2'));
                    if isempty(strfind(line,'='))
                        append_this = ['=' line];
                    else
                        append_this = ['.' append_this_1 '=''' append_this_2 ''''];
                    end
                    
                    % put it in the tree structure
                    eval([obj.current_property_tree append_this ';'])
            end
        end
        
        % calculate the coordinates based on encoding_matrix_coordinates
        function obj = getCoords(obj)
            % obj = getCoords(obj,coord_start,coord_offsets)
            coords = zeros(2,obj.contours(obj.current_contour_id).nr_coords);
            for ic_sub = 1:obj.contours(obj.current_contour_id).nr_coords
                enc_nr = str2double(obj.contours(obj.current_contour_id).coord_offsets(ic_sub));
                [x,y] = find(obj.encoding_matrix_coordinates==enc_nr);
                x = x-2;
                y = y-2;
                if ic_sub == 1
                    coords(:,1) = obj.contours(obj.current_contour_id).coord_start' + [x;y];
                else
                    coords(:,ic_sub) = coords(:,ic_sub-1) + [x;y];
                end
            end
            % correct for scaling and start counting at 1 in matlab
            coords = coords / obj.contours(obj.current_contour_id).scaling + 1;
            obj.contours(obj.current_contour_id).coordinates  = coords;
        end
        
        % set the file version - only 1.0 is supported
        function obj = setVersion(obj,line)
            % obj = setVersion(obj,line)
            [property_name,property_value] = splitPropertyValue(line);
            if strcmp(property_name, 'Version')
                if strcmp(property_value,'1.0')
                    obj.fileversion = property_value;
                else
                    error('Unexpected file version. Only vesselmass file version 1.0 is supported.')
                end
            else
                error('Version not found. Unexpected file format')
            end
        end
    end
    
end

% some basic functions not requiring the class object
function [property_name,property_value] = splitPropertyValue(line)
property_name = regexprep(line,'(.*)=(.*)','$1');
property_value = regexprep(line,'(.*)=(.*)','$2');
end
