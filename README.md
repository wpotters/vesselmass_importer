# README #
**vesselmass_importer** Matlab class to quickly import contour files (*.con) created by Medis VesselMass for visualisation with Matlab.

Only tested with version 1.0 contour files.

[Click here to go to the repository download section. Here you can download the Matlab source code.](https://bitbucket.org/wpotters/vesselmass_importer/downloads)

### To fetch coordinates:

```
#!matlab
filename = '/path/to/contourfile.con';         % CHANGEME
obj = vesselmass_importer(filename);           % load file
coordinates2D = obj.getContourCoordinates();   % 2d x,y
coordinates3D = obj.getContourCoordinates3d(); % 3d with slice#

```

### Problems with slice order
If you encounter issues with the slice ordering; try inverting the slice order:
```
#!matlab
filename = '/path/to/contourfile.con';         % CHANGEME
obj = vesselmass_importer(filename);           % load file
obj.invert_slice_order = true;                 % invert slice order
coordinates3D = obj.getContourCoordinates3d(); % 3d with inverted slice#

```

### Other data from *.con file: 
Other data from the text file is stored as a structure in the following properties: *main_properties, study_information, mass_mode, quantification_mode, contour_detection, display_local_info display_global_info*